package serviceRequest;

import org.testng.Assert;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class getData {
	@Test
    public void test_api(){
        given().
                when().
                    get("http://ergast.com/api/f1/2017/circuits.json").
                then().
                    assertThat().
                    body("MRData.CircuitTable.Circuits.circuitId",hasSize(20));
        given().
                when().
                get("http://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=439d4b804bc8187953eb36d2a8c26a02").
                then().
                assertThat().
                body("name",equalTo("London"));
    }
	
}
